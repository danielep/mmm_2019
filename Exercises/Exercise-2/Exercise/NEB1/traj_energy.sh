
#remove the file if it exist
if [ -f trajectory.xyz ]
then
   rm trajectory.xyz
fi

#create trajectory taking the final position of aech replica
for i in `seq 1 8`
   do tail -n 40 output_neb-pos-Replica_nr_${i}-1.xyz >> trajectory.xyz
done

#create energy file
grep E trajectory.xyz | awk '{print NR, $6}' > energy.dat
